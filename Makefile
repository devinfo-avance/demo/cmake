
all : Demo

DEBUG_FLAGS= -O0 -g
PROD_FLAGS= -O3
MODE_FLAGS=$(DEBUG_FLAGS)


OBJS = ./src/helloworld.o ./src/test.o 

CFLAGS = -W -Wall -Wextra -Wwrite-strings -Wconversion -std=gnu99 -fpie -Werror

%.o : %.c %.h
	gcc $(MODE_FLAGS) $(CFLAGS) -c $< -o $@

%.o : %.c 
	gcc $(MODE_FLAGS) $(CFLAGS) -c $< -o $@

Demo : src/demo.c $(OBJS)
	gcc $(MODE_FLAGS) $(CFLAGS) $^ -o ./bin/$@

clean : 
	rm ./Demo 
	rm $(OBJS)

